The primary featureset of OBS Studio is composed of the following:

 - Composite scenes
 - Stream
 - Record

Secondarily, OBS Studio supports the following features:

 - Virtual camera
 - Services integration


# Concepts

## Source

A source is anything that can generate multimedia data for OBS Studio to
compose. It is the basic unit of a scene. Some examples of sources are screen
captures, static images, videos, microphones, etc.

## Group

A group is a container of sources. It groups sources as if they are one.

## Scene

A scene is a collection of sources.

## Filters

Filters are post-processing effects applied to scenes or sources.

## Profile

A profile is a set of settings, such as output resolution, audio sample rate,
etc. It is independent of scenes.
