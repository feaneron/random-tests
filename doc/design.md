# Exploring the Broadcasting Design Space

If we were to design a broadcast station in a software box, what does that
entail?

## Goals

The minimum viable product is being able to mix audio and video inputs and
stream it to a service like Youtube or Twitch. It requires a setup process for
the service and means to capture and mix sound from microphones and other
sources, screen caputure (monitor / window / area) and video devices. A single
scene streaming is already an MVP. This can be further extended:

+ Stream health monitoring. No point putting effort into performance when the
  internets go down.

+ Audio monitor and mixer. Audio is 90% of a successful stream. Video can be
  choppy and terrible and still passable when the sound is good. In OBS the
  audio mix is global. *Explore use cases for AFP (audio follows video), ie. per
  scene audio setups.*

+ Switch between *scenes*. A scene is a pre-made layout of video and audio
  inputs to best communicate a certain activity. A software tutorial may require
  a display capture & overlaid camera head shot. A musician may want to have a
  scene with an overhead camera showing the hands on a keyboard during a
  performance and a super tight shot of a synth screen. There is a good chance
  of covering 80% of such scenarios with default templates. Or a set of activity
  related templates  --  **gaming**, **music performance**, **crafts** or an
  **interview**. 

+ Scene editor. Allow to customize or create new scenes to fit a more niche use
  case. This can either be done by picking a template format that is editable
  with existing tools (for example an SVG template editable with Inkscape), by
  having a separate scene editor app or an editing mode in the Broadcaster. The
  former approach has the biggest barrier to entry, but could open up room for
  template catalogues / marketplaces possibly integrating it into the scene set
  selector. *Investigate use cases to editing scenes during a live broadcast*

## Prior Art

- Hardware broadcasting solutions: [Blackmagick ATEM][blackmagic-atem],
  particularly the entry level Mini targeting single person use.

### Scene Editor Considerations

- Position, Scale, (Rotation?) of input; stacking/z-order
- Video: looping, masks (overlaid head in a circle), keying (green screen).
- Special state to consider is a '**will be right back** for a toilet break,
  technical difficulties, kids entering the room naked, etc :) ).

## Initial Service Setup

- Set up service. Prioritize 3 mainstream services with sane defaults and lots
  of hand holding rather than putting all on the same level of importance and
  making it suck for all of them. Think of gmail/exchange in Geary requiring
  minimal user input, compared to a generic IMAP post office.

## Feedback

- Good streams are built on a feedback loop. The broadcaster is usually a single
  person and managing everything can be overwhelming. Provide tools to engage
  with the audience. Make comments made by the audience as visible as the status
  of the stream going out. Latency is a big enemy. *Might be good to have the
  comments directly overlaid in the video itself, as it's the primary feedback
  for the broadcaster what is being shown and being constantly checked but also
  indicates latency to the audience.*

## Activity related considerations

Gaming is pretty specific because it's super focused / fullscreen activity by
the same person wanting to engage with audience. On a single device that's
likely only possible on a dualhead setup or using a secondary device and a
capture card.

## Periodic Streamer Quality of Life

- Schedule and notifications. Scheduling and agenda management might be cool to
  have.




[blackmagic-atem]: https://www.blackmagicdesign.com/products/atem
