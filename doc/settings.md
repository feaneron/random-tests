# Goal

Make the settings dialog easier and simpler to understand, while retaining the
same (or most of the) preferences it currently has.

# How

Reorganize the settings dialog by:

 - Regrouping preferences
 - Restructuring the pages
 - Replacing some settings by different in-app behaviors
 
Throughout here, I assumed that some of these preferences would be offloaded
into a separate application that would handle pro-production (eSports, shows,
big productions, etc) cases. There are no mockups for such application yet.

# Settings

## General

 - **General**
   - **Language**: use system default? Remove combo?
   - **Theme**: move to a separate page, add preview
   - **Open statistics**: remove; remember last state
   - **Automatic updates**: keep, add a description, disable on mobile data

 - **Output**
   - **Ask before ...**: ???
   - **Record when streaming**: ???
   - **Continue recording after streaming stopped**: ???
   - **Start repeat buffer when streaming**: ???
   - **Keep repeat buffer on after streaming**: remove? always on

 - **Source Snapping**: →→→ move to new Scenes & Sources page
   - **Enabled**: -
   - **Sensitivity**: -
   - **Snap to screen edge**: -
   - **Snap to other sources**: -
   - **Snap to vertical and horizontal center**: -

 - **System Tray**: hide when systray is not available
   - **Enabled**: ???
   - **Start minimized to tray**: ???
   - **Always minimize to tray**: remove? When systray is on, close → systray

 - **Projectors**: ???
   - **Hide cursor over projectors**: ???
   - **Make projectors always on top**: ???
   - **Save projectors on exit**: ???
   - **Limit one fullscreen projector per screen**: ???

 - **Preview**: →→→ move to new Scenes & Sources page
   - **Hide overflow**: invert to "Show overflow", group with Source Snapping
   - **Overflow always visible**: ??? (Doesn't this conflict with the above?)
   - **Show overflow even when source is invisible**: ???
   - **Draw safe areas (EBU R 95)**: keep; needs better description

 - **Importers**: →→→ move to Import dialog
   - **Search known locations for scene collections when importing**: -

 - **Studio Mode**: →→→ move to new Scenes & Sources page
   - **Transition to scene when double-clicked**: -
   - **Enable vertical layout**: -
   - **Show preview labels**: -

 - **Multiview**: →→→ move to new Scenes & Sources page
   - **Click to switch between scenes**: -
   - **Show scene names**: -
   - **Draw safe areas (EBU R 95)**: -
   - **Layout**: -

## Stream

Move the entire page to a separate dialog. Reason: this is a core feature
prominent in the main interface, not a user preference.

## Audio

 - **General**
   - **Sample Rate**: keep
   - **Channels**: keep

 - **Global Audio Devices**
   - **Desktop Audio ...**: ???
   - **Mic/Auxiliary Audio ...**: ???

 - **Meters**: add preview
   - **Decay Rate**: keep
   - **Peak Meter Type**: keep; needs description

 - **Advanced**: remove
   - **Monitoring Device**: move to General section

 - **Hotkeys**: →→→ move to Hotkeys page

## Video

 - **Base (Canvas) Resolution**: keep; rename to Resolution
 - **Output (Scaled) Resolution**: move to Encoding section; split between Streaming and Recording
 - **Downscale Filter**: keep?
 - **FPS**: keep? might need rework

## Output

Rename page to "Encoding"?

 - **Output Mode**: remove (no replacement)

 - **Streaming**
   - **Audio Track**: keep?
   - **Encoder**: keep
   - **Rescale Output**: what's the difference between this and Stream
       Resolution? H4ndy says:
       > It's a workaround to the limitation that OBS only has one resolution output
       > it was supposed to be reworked with the output rework (which hasn't happen yet)

 - **Recording**
   - **Type**: keep?
   - **Recording Path**: keep
   - **Generate File Name without Space**: ???
   - **Recording Format**: keep?
   - **Audio Track**: keep?
   - **Encoder**: keep?
   - **Custom Muxer Settings**: ???

 - **Audio**: maybe move to the Audio page
   - **Track ...**: keep?

 - **Replay Buffer**
   - **Enable Replay Buffer**: keep; move somewhere else? needs description

## Hotkeys

Rename panel to "Keyboard Shortcuts"? Vaguely recall it's a more recognized
term. Merge search fieds, search both for the shortcut name and the key trigger.

## Advanced

Please let's just break down and remove this page entirely.

 - **General**
   - **Show exit notification**: ???

 - **Video**: →→→ move to Video page
   - **Color Format**: -
   - **Color Space**: -
   - **Color Range**: -

 - **Recording**
   - **Filename Formatting**: ???
   - **Overwrite if file exists**: ???
   - **Automatically remux to mp4**: ???
   - **Replay Buffer Filename Prefix**: ???

 - **Stream Delay**: →→→ move to General page
   - **Enable**: ???
   - **Duration**: ???
   - **Preserve cutoff point when reconnecting**: excise into a separate app?

 - **Automatically Reconnect**: →→→ move to new Network page
   - **Enable**: -
   - **Retry Delay**: -
   - **Maximum Retries**: -

 - **Newtork**: →→→ move to new Network page
   - **Bind to IP**: -
   - **Dynamically change bitrate to manage congestion**: -

 - **Hotkeys**: →→→ move to new Keyboard Shortcuts page
   - **Focus behavior**: -
