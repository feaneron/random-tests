# Goal

Minimize the number of steps of the initial setup process. Make it closer to a
welcome than a questionnaire.

# Justification

The initial setup is the first interaction people have with the application. It
sets the tone for following interactions, and can either welcome and make people
feel at home, or scare them away.

In general, asking questions when onboarding should be avoided. It might overwhelm
people, especially those who never used the application, with questions they may
not know an answer to.

# How

Specify the following:

 - What are the questions OBS Studio *must* ask? What information must be
   provided by the user, cannot be automatically detected, which OBS cannot
   operate without?

## Current Flows

TODO

## Proposed

 1. **Welcome**: this sets a lightweight, warmer tone to OBS Studio. It
    provides context and orientation for what the next steps will be, reduces
    the "unknown" factor.

 2. **Streaming**: optional, can skip. Can log in to various streaming services.
 
 3. **Optimize**: sets screen resolution, FPS. If streaming services are set,
    also perform a network test.

 4. **Thank You**: another form-over-function, tone-setting page. It marks the
    end of the onboarding process, and let people know that that's the point
    where they'll start using OBS.
