
const { Adw, GLib, GObject, Gtk } = imports.gi;

var ObsNgVolumeBar = GObject.registerClass({
    GTypeName: 'ObsNgVolumeBar',
    Template: 'resource:///com/obsproject/StudioNG/volume.ui',
    CssName: 'volumebar',
    InternalChildren: ['muteButton'],
}, class ObsNgVolumeBar extends Gtk.Box {
    _init(params = {}) {
        super._init(params);

        this._muteButton.connect('clicked', () => {
            const { active } = this._muteButton;
            this._muteButton.iconName = active
                ? 'audio-volume-muted-symbolic'
                : 'audio-volume-high-symbolic';

            if (active) {
                this._muteButton.add_css_class('destructive-action');
            } else {
                this._muteButton.remove_css_class('destructive-action');
            }
        });
    }
});

