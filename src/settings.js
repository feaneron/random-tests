
const { Adw, Gdk, GLib, GObject, Gtk } = imports.gi;

const ThemeSelector = imports.themeSelector;

var ObsNgStyleSelector = GObject.registerClass({
    GTypeName: 'ObsNgStyleSelector',
    CssName: 'styleselector',
    Template: 'resource:///com/obsproject/StudioNG/styleSelector.ui',
    InternalChildren: ['dark', 'follow', 'light'],
}, class ObsNgStyleSelector extends Adw.Bin {
    _init(params = {}) {
        super._init(params);

        const styleManager = Adw.StyleManager.get_default();
        if (styleManager.colorScheme == Adw.ColorScheme.DEFAULT)
            this._follow.active = true;
        else if (styleManager.colorScheme == Adw.ColorScheme.PREFER_DARK ||
                 styleManager.colorScheme == Adw.ColorScheme.FORCE_DARK)
            this._dark.active = true;
        else
            this._light.active = true;
    }

    _onCheckActiveChangedCb() {
        const styleManager = Adw.StyleManager.get_default();

        if (this._follow.active)
            styleManager.colorScheme = Adw.ColorScheme.DEFAULT;
        else if (this._dark.active)
            styleManager.colorScheme = Adw.ColorScheme.FORCE_DARK;
        else if (this._light.active)
            styleManager.colorScheme = Adw.ColorScheme.FORCE_LIGHT;
    }
});

const SHORTCUTS = [
    {
        title: 'Toggle Streaming',
    },
    {
        title: 'Toggle Recording',
    },
    {
        title: 'Toggle Replay Buffer',
    },
    {
        title: 'Toggle Virtual Camera',
    },
];

const ShortcutRow = GObject.registerClass({
    GTypeName: 'ObsNgShortcutRow',
}, class ObsNgShortcutRow extends Adw.ActionRow {
    _init(shortcut) {
        super._init({
            title: shortcut.title,
        });
    }
});

var ObsNgSettingsDialog = GObject.registerClass({
    GTypeName: 'ObsNgSettingsDialog',
    Template: 'resource:///com/obsproject/StudioNG/settings.ui',
    InternalChildren: ['shortcutsListbox'],
}, class ObsNgSettingsDialog extends Adw.Window {
    _init(params = {}) {
        ThemeSelector.ObsNgThemeSelector.$gtype;
        ObsNgStyleSelector.$gtype;

        super._init(params);

        for (const shortcut of SHORTCUTS)
            this._shortcutsListbox.append(new ShortcutRow(shortcut));
    }
});

