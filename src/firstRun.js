
const { Adw, GLib, GObject, Gtk } = imports.gi;

var ObsNgFirstRunDialog = GObject.registerClass({
    GTypeName: 'ObsNgFirstRunDialog',
    Template: 'resource:///com/obsproject/StudioNG/firstRun.ui',
    InternalChildren: [
        'carousel',
        'cancelButton',
        'letsGoButton',
        'nextButton',
        'previousButton',
        'startUsingObsButton',
    ],
}, class ObsNgFirstRunDialog extends Adw.ApplicationWindow {
    _init(application) {
        super._init({ application });

        this._cancelButton.connect('clicked', () => this.close());
        this._startUsingObsButton.connect('clicked', () => this.close());
        this._previousButton.connect('clicked', () => {
            const previousPageIndex = Math.ceil(this._carousel.position) - 1;
            this._goToPage(previousPageIndex);
        });
        this._nextButton.connect('clicked', () => {
            const nextPageIndex = Math.floor(this._carousel.position) + 1;
            this._goToPage(nextPageIndex);
        });
        this._letsGoButton.connect('clicked', () => this._goToPage(1));

        this._goToPage(0);
    }

    _goToPage(index) {
        const { nPages } = this._carousel;

        index = Math.min(Math.max(index, 0), nPages - 1);

        const isFirstPage = index === 0;
        const isLastPage = index === nPages - 1;

        this._previousButton.visible = !isFirstPage && !isLastPage;
        this._nextButton.visible = !isFirstPage && !isLastPage;

        const page = this._carousel.get_nth_page(index);
        this._carousel.scroll_to(page, true);
    }
});
