
const { Adw, GLib, GObject, Gtk } = imports.gi;

var ObsNgServicesDialog = GObject.registerClass({
    GTypeName: 'ObsNgServicesDialog',
    Template: 'resource:///com/obsproject/StudioNG/services.ui',
    InternalChildren: [],
}, class ObsNgServicesDialog extends Adw.Window {
    _init(params = {}) {
        super._init(params);
    }
});
