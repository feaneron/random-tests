
const { Adw, GLib, GObject, Gtk } = imports.gi;

const THEMES = [
    {
        name: 'System',
        styleClass: undefined,
    },
    {
        name: 'Acri',
        styleClass: 'acri',
    },
    {
        name: 'Adwaita',
        styleClass: 'adwaita',
    },
    {
        name: 'Rachni',
        styleClass: 'rachni',
    },
    {
        name: 'Solarized',
        styleClass: 'solarized',
    },
];

const ThemeCard = GObject.registerClass({
    GTypeName: 'ObsNgThemeCard',
    CssName: 'themecard'
}, class ThemeCard extends Gtk.FlowBoxChild {
    _init(theme) {
        const box = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            spacing: 6,
        });

        super._init({ child: box });

        const bin = new Adw.Bin({
            cssClasses: ['card', theme.styleClass ?? ''],
        });
        box.append(bin);

        const overlay = new Gtk.Overlay();
        bin.child = overlay;

        const selectedIcon = new Gtk.Image({
            iconName: 'object-select-symbolic',
            cssClasses: ['selected-icon'],
            halign: Gtk.Align.END,
            valign: Gtk.Align.END,
        })
        overlay.add_overlay(new Adw.Bin({
            child: selectedIcon,
            cssClasses: ['card', 'osd'],
        }));

        box.append(new Gtk.Label({
            label: theme.name,
        }));
    }

    applyColorScheme() {
        // TODO
    }
});

var ObsNgThemeSelector = GObject.registerClass({
    GTypeName: 'ObsNgThemeSelector',
    Template: 'resource:///com/obsproject/StudioNG/themeSelector.ui',
    InternalChildren: ['flowbox'],
}, class ObsNgThemeSelector extends Gtk.Box {
    _init(params = {}) {
        super._init(params);

        for (const theme of THEMES)
            this._flowbox.append(new ThemeCard(theme));
    }

    _onFlowSelectedChildrenChangedCb() {
        const selected = this._flowbox.get_selected_children()[0];
        selected.applyColorScheme();
    }
});
