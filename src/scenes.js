
const { Adw, GLib, GObject, Gtk } = imports.gi;

var ObsNgScenesSidebar = GObject.registerClass({
    GTypeName: 'ObsNgScenesSidebar',
    Template: 'resource:///com/obsproject/StudioNG/scenes.ui',
    InternalChildren: [],
}, class ObsNgScenesSidebar extends Gtk.Widget {
    _init(params = {}) {
        super._init(params);
    }
});
