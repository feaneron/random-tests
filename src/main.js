
pkg.initGettext();
pkg.initFormat();
pkg.require({
    'Adw': '1',
    'Gio': '2.0',
    'Gtk': '4.0',
    'Panel': '1',
});

const { Adw, Gdk, Gio, Gtk, Panel } = imports.gi;

const { ObsNgWindow } = imports.window;
const FirstRun = imports.firstRun;

function main(argv) {
    const application = new Adw.Application({
        application_id: 'com.obsproject.StudioNG',
        flags: Gio.ApplicationFlags.FLAGS_NONE,
    });

    application.connect_after('startup', app => {
        Panel.init();

        // Dark
        const styleManager = Adw.StyleManager.get_default();
        styleManager.colorScheme = Adw.ColorScheme.FORCE_DARK;
    });
    application.connect('activate', app => {
        let activeWindow = app.activeWindow;
        
        if (!activeWindow) {
            activeWindow = new FirstRun.ObsNgFirstRunDialog(app);
            activeWindow.connect('close-request', () => {
                const window = new ObsNgWindow(app);
                window.present();
                return Gdk.EVENT_PROPAGATE;
            });
        }

        activeWindow.present();
    });

    return application.run(argv);
}
