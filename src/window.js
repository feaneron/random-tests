
const { Adw, Gdk, GLib, GObject, Gtk } = imports.gi;

const Scenes = imports.scenes;
const Services = imports.services;
const Settings = imports.settings;
const Volume = imports.volume;

function twoDigits(number) {
    return number.toLocaleString('en-US', {
        minimumIntegerDigits: 2,
        useGrouping: false
      });
}

var ObsNgWindow = GObject.registerClass({
    GTypeName: 'ObsNgWindow',
    Template: 'resource:///com/obsproject/StudioNG/window.ui',
    InternalChildren: [
        'connectServiceButton',
        'editDock',
        'editDockSwitchersBox',
        'mainStack',
        'recordButton',
        'recordDurationLabel',
        'settingsButton',
        'streamBitrateLabel',
        'streamButton',
        'streamDock',
        'streamDurationLabel',
        'streamQualityIcon',
        'streamStatusBox',
        'titleStack',
        'viewButton',
    ],
}, class ObsNgWindow extends Adw.ApplicationWindow {
    _init(application) {
        Scenes.ObsNgScenesSidebar.$gtype;
        Volume.ObsNgVolumeBar.$gtype;

        super._init({ application });
        
        this._settingsButton.connect('clicked', () => {
            const dialog = new Settings.ObsNgSettingsDialog({
                modal: true,
                transientFor: this,
            });
            dialog.present();
        });
        
        this._streamButton.connect('notify::active', () => {
            const { active } = this._streamButton;
            
            const pageName = active ? 'streaming' : 'idle';
            
            if (active)
                this._startStream();
            else
                this._endStream();
        });
        
        this._recordButton.connect('notify::active', () => {
            const { active } = this._recordButton;
            
            if (active) {
                this._startTimerLabel(this._recordDurationLabel);
                this._recordButton.add_css_class('destructive-action');
            } else {
                this._endTimerLabel(this._recordDurationLabel);
                this._recordButton.remove_css_class('destructive-action');
            }
        });

        this._connectServiceButton.connect('clicked', () => {
            const dialog = new Services.ObsNgServicesDialog({
                modal: true,
                transientFor: this,
            });
            dialog.connect('close-request', () => {
                this._titleStack.visible_child_name = 'services';
                this._streamButton.sensitive = true;
                return Gdk.EVENT_PROPAGATE;
            });
            dialog.present();
        });
    }

    _onViewButtonRowActivatedCb(listbox, row) {
        const rowIndex = row.get_index();

        this._setMainStackPage(['scenes-editor', 'stream-controls'][rowIndex]);
        this._viewButton.popdown();
    }
    
    _startStream() {
        this._startTimerLabel(this._streamDurationLabel);
        this._streamButton.add_css_class('suggested-action');
        this._setMainStackPage('stream-controls');

        this._updateStreamTimerId =
            GLib.timeout_add(GLib.PRIORITY_DEFAULT, 2500, () => {
                const bitrate = Math.floor(Math.random() * 6000);
                const quality = 5 * bitrate / 6000;

                ['error', 'warning', 'success'].forEach(
                    cssClass => this._streamQualityIcon.remove_css_class(cssClass));

                if (quality < 1)
                    this._streamQualityIcon.add_css_class('error');
                else if (quality < 3)
                    this._streamQualityIcon.add_css_class('warning');
                else
                    this._streamQualityIcon.add_css_class('success');

                this._streamBitrateLabel.label = `${bitrate} KB/s`;

                if (quality < 1)
                    this._streamQualityIcon.iconName = 'network-cellular-signal-none-symbolic';
                else if (quality < 2)
                    this._streamQualityIcon.iconName = 'network-cellular-signal-weak-symbolic';
                else if (quality < 3)
                    this._streamQualityIcon.iconName = 'network-cellular-signal-ok-symbolic';
                else if (quality < 4)
                    this._streamQualityIcon.iconName = 'network-cellular-signal-good-symbolic';
                else
                    this._streamQualityIcon.iconName = 'network-cellular-signal-excellent-symbolic';
                
                return GLib.SOURCE_CONTINUE;
            });
    }
    
    _endStream() {
        this._endTimerLabel(this._streamDurationLabel);
        this._streamButton.remove_css_class('suggested-action');
        this._streamQualityIcon.iconName = 'network-cellular-signal-none-symbolic';
        this._setMainStackPage('scenes-editor');

        ['error', 'warning', 'success'].forEach(
            cssClass => this._streamQualityIcon.remove_css_class(cssClass));
        this._streamBitrateLabel.label = `— KB/s`;

        GLib.source_remove(this._updateStreamTimerId);
        delete this._updateStreamTimerId;
    }

    _startTimerLabel(label) {
        label._ellapsedSeconds = 0;
        label._tickId =
            GLib.timeout_add(GLib.PRIORITY_DEFAULT, 1000, () => {
                const elapsed = ++label._ellapsedSeconds;
                this._formatTimerLabel(label, elapsed);
                
                return GLib.SOURCE_CONTINUE;
            });
    }
    
    _endTimerLabel(label) {
        if (!label._tickId)
            return;

        this._formatTimerLabel(label, 0);
        GLib.source_remove(label._tickId);
        delete label._tickId;
    }
    
    _formatTimerLabel(label, elapsedTime) {
        const seconds = elapsedTime % 60;
        const minutes = Math.floor(elapsedTime / 60) % 60;
        const hours = Math.floor(elapsedTime / (60 * 60));
        label.label = `${twoDigits(hours)}:${twoDigits(minutes)}:${twoDigits(seconds)}`;
    }

    _setMainStackPage(page) {
        switch (page) {
        case 'scenes-editor':
            this._viewButton.iconName = 'document-edit-symbolic';
            this._mainStack.visibleChild = this._editDock;
            this._editDockSwitchersBox.show();
            break;
        case 'stream-controls':
            this._viewButton.iconName = 'network-cellular-symbolic';
            this._mainStack.visibleChild = this._streamDock;
            this._editDockSwitchersBox.hide();
            break;
        }
    }
});

